package main

import (
	"runtime"

	"gitlab.com/fredcarle/fredo-cms/commands"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	commands.StartFredo()
}
