package controller

import (
	"log"
	"net/http"
	"regexp"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"

	"github.com/labstack/echo"

	"gitlab.com/fredcarle/fredo-cms/model"
	"gitlab.com/fredcarle/fredo-cms/utils/config"
	"gitlab.com/fredcarle/fredo-cms/utils/session"
	"gitlab.com/fredcarle/fredo-cms/utils/url"
)

// SinglePost is used to show a single post
func SinglePost(c echo.Context) error {
	d := new(model.SinglePost)

	d.Post.Slug = c.Param("slug")
	d.Post.GetSinglePost()
	d.SiteURL = config.SiteURL
	d.Post.Content = strings.Replace(d.Post.Content, "<!--more-->", "", 1)
	d.Post.Comments = model.GetComments(d.Post.Slug)

	return c.Render(http.StatusOK, config.TemplateName+"/singlepost", d)
}

// ShowPosts shows the list of editable posts
func ShowPosts(c echo.Context) error {
	d := new(model.AllPosts)
	d.Posts = model.GetAllPostsForAdmin()
	d.SiteURL = config.SiteURL
	return c.Render(http.StatusOK, "admin/posts", d)
}

// NewPost shows the new post form
func NewPost(c echo.Context) error {
	d := new(model.PostEdit)
	d.Post.ID = bson.NewObjectId()
	d.Post.Status = "draft"
	d.SiteURL = config.SiteURL
	d.Post.Author.Username = session.CheckAuthorUsername(c)
	model.GetAuthor(&d.Post.Author)
	d.New = true
	return c.Render(http.StatusOK, "admin/editpost", d)
}

// EditPost show the post editing form
func EditPost(c echo.Context) error {
	d := new(model.PostEdit)
	d.Post.ID = bson.ObjectIdHex(c.Param("id"))
	d.Post.GetSinglePost()
	d.SiteURL = config.SiteURL
	d.New = false
	return c.Render(http.StatusOK, "admin/editpost", d)
}

// SavePost saves the post in the database and returns the user to the
// main admin page
func SavePost(c echo.Context) error {

	post := new(model.Post)
	post.ID = bson.ObjectIdHex(c.Param("id"))
	post.Title = c.FormValue("Title")
	post.Status = c.FormValue("Status")
	post.Author.Username = c.FormValue("AuthorUsername")
	model.GetAuthor(&post.Author)
	post.Categories = append(post.Categories, c.FormValue("Category"))
	post.Content = c.FormValue("Content")

	form, err := c.MultipartForm()
	if err != nil {
		log.Printf("[IMAGE UPLOAD] Error while trying to upload images: %v", err)
	}

	if form != nil {
		post.Images, err = SaveImagesToDisk(form)
		if err != nil {
			log.Printf("[IMAGE UPLOAD] Error while trying to upload images: %v", err)
		}
	}

	post.Date = time.Now()
	post.Slug = url.PrettyURL(post.Title)
	if post.Title == "" {
		d := new(model.SinglePost)
		d.SiteURL = config.SiteURL
		d.Post = *post

		return c.Render(http.StatusBadRequest, "admin/editpost", d)
	}

	del := c.FormValue("Delete")
	matched, _ := regexp.MatchString("^(?i)delete$", del)
	switch matched {
	case true:
		model.DeletePost(post.ID)
		return c.Redirect(http.StatusFound, "/admin/posts")
	}

	post.SavePost()

	return c.Redirect(http.StatusFound, "/admin/posts")
}
