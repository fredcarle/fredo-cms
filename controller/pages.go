package controller

import (
	"net/http"
	"regexp"
	"time"

	"github.com/labstack/echo"
	"gitlab.com/fredcarle/fredo-cms/model"
	"gitlab.com/fredcarle/fredo-cms/utils/config"
	"gitlab.com/fredcarle/fredo-cms/utils/session"
	"gitlab.com/fredcarle/fredo-cms/utils/url"
	"gopkg.in/mgo.v2/bson"
)

// SinglePage is used to show a single page
func SinglePage(c echo.Context) error {
	d := new(model.SinglePage)
	d.Page.Slug = c.Param("page")
	d.Page.GetSinglePage()
	d.SiteURL = config.SiteURL

	return c.Render(http.StatusOK, config.TemplateName+"/page", d)
}

// ShowPages shows the list of editable pages
func ShowPages(c echo.Context) error {
	d := new(model.AllPages)
	d.Pages = model.GetAllPages()
	d.SiteURL = config.SiteURL
	return c.Render(http.StatusOK, "admin/pages", d)
}

// NewPage shows the new page form
func NewPage(c echo.Context) error {
	d := new(model.PageEdit)
	d.Page.ID = bson.NewObjectId()
	d.Page.Status = "draft"
	d.SiteURL = config.SiteURL
	d.Page.Author.Username = session.CheckAuthorUsername(c)
	model.GetAuthor(&d.Page.Author)
	d.New = true
	return c.Render(http.StatusOK, "admin/editpage", d)
}

// EditPage show the page editing form
func EditPage(c echo.Context) error {
	d := new(model.PageEdit)
	d.Page.ID = bson.ObjectIdHex(c.Param("id"))
	d.Page.GetSinglePage()
	d.SiteURL = config.SiteURL
	d.New = false
	return c.Render(http.StatusOK, "admin/editpage", d)
}

// SavePage saves the page in the database and returns the user to the
// main admin page
func SavePage(c echo.Context) error {

	page := new(model.Page)
	page.ID = bson.ObjectIdHex(c.Param("id"))
	page.Title = c.FormValue("Title")
	page.Status = c.FormValue("Status")
	page.Author.Username = session.CheckAuthorUsername(c)
	model.GetAuthor(&page.Author)
	page.Content = c.FormValue("Content")
	page.Date = time.Now()
	page.Slug = url.PrettyURL(page.Title)
	if page.Title == "" {
		d := new(model.SinglePage)
		d.SiteURL = config.SiteURL
		d.Page = *page

		return c.Render(http.StatusBadRequest, "admin/editpage", d)
	}

	del := c.FormValue("Delete")
	matched, _ := regexp.MatchString("^(?i)delete$", del)
	switch matched {
	case true:
		model.DeletePost(page.ID)
		return c.Redirect(http.StatusFound, "/admin/pages")
	}

	page.SavePage()

	d := new(model.AllPages)
	d.Pages = model.GetAllPages()
	d.SiteURL = config.SiteURL

	return c.Render(http.StatusOK, "admin/pages", d)
}
