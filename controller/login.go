package controller

import (
	"html/template"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/fredcarle/fredo-cms/model"
	"gitlab.com/fredcarle/fredo-cms/utils/config"
	"gitlab.com/fredcarle/fredo-cms/utils/hash"
	"gitlab.com/fredcarle/fredo-cms/utils/session"
)

// Login shows the login page
func Login(c echo.Context) error {
	d := struct {
		SiteURL string
		Message string
	}{config.SiteURL, ""}
	if session.ValidateToken(c) {
		return c.Redirect(http.StatusFound, "/admin")
	}
	// var d struct {
	// 	SiteURL string
	// 	IsUser  bool
	// }
	// d.SiteURL = config.SiteURL
	// d.IsUser = false
	// a := new(model.Author)
	// session.CheckAuthorUsername(a, c)
	// if a.Username == "" {
	// 	return c.Render(http.StatusOK, "admin/login", d)
	// }
	//
	// d.IsUser = true
	return c.Render(http.StatusOK, "admin/login", d)
}

// LoginUser lets the user login if username and password are validated
func LoginUser(c echo.Context) error {
	u := new(model.User)
	u.Username = c.FormValue("Username")

	if u.Username == "" || c.FormValue("Password") == "" {
		d := struct {
			SiteURL string
			User    *model.User
			Message template.HTML
		}{
			config.SiteURL,
			&model.User{Username: u.Username},
			`<div class="errorMessage">
			    <p>
			        Please enter username and password.
			    </p>
			</div>`,
		}

		return c.Render(http.StatusBadRequest, "admin/login", d)
	}

	err := u.GetUserPassword()
	if err != nil || !validatePassword(u, c.FormValue("Password")) {
		d := struct {
			SiteURL string
			User    model.User
			Message template.HTML
		}{
			config.SiteURL,
			model.User{Username: u.Username},
			`<div class="errorMessage">
			    <p>
			        Invalid username and/or password.
			    </p>
			</div>`,
		}
		c.Render(http.StatusBadRequest, "admin/login", d)
	}

	u.GetUserRole()

	tokenString := session.CreateToken(u)

	c.SetCookie(session.CreateCookie(tokenString))

	redirection := session.ReadRedirectionCookie(c)
	if redirection != "" {
		return c.Redirect(http.StatusFound, redirection)
	}

	return c.Redirect(http.StatusFound, "/admin")

}

func validatePassword(u *model.User, password string) bool {
	return hash.Match(u.Password, password)
}

// Logout logs the user out by invalidating the cookie holding the JWT token
func Logout(c echo.Context) error {
	c.SetCookie(session.InvalidateCookie("Thanks for visiting"))
	return c.Redirect(http.StatusFound, "/")
}
