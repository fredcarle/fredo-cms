package controller

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/fredcarle/fredo-cms/model"
	"gitlab.com/fredcarle/fredo-cms/utils/config"
)

// Admin shows the main admin page
func Admin(c echo.Context) error {
	pageCount := model.PageCount()
	postCount := model.PostCount()
	userCount := model.UserCount()
	commentCount := model.CommentCount()
	d := struct {
		PageCount    int
		PostCount    int
		UserCount    int
		CommentCount int
		SiteURL      string
	}{
		pageCount,
		postCount,
		userCount,
		commentCount,
		config.SiteURL,
	}
	return c.Render(http.StatusOK, "admin/dashboard", d)
}
