package controller

import (
	"html/template"
	"log"
	"net/http"
	"time"

	"github.com/labstack/echo"
	"gitlab.com/fredcarle/fredo-cms/model"
	"gitlab.com/fredcarle/fredo-cms/utils/config"
	"gitlab.com/fredcarle/fredo-cms/utils/hash"
	"gitlab.com/fredcarle/fredo-cms/utils/mail"
	"gitlab.com/fredcarle/fredo-cms/utils/session"
	"gopkg.in/mgo.v2/bson"
)

// ForgotPassword shows the forgot passwerd page
func ForgotPassword(c echo.Context) error {
	d := struct {
		SiteURL string
		Message string
	}{config.SiteURL, ""}
	if session.ValidateToken(c) {
		return c.Redirect(http.StatusFound, "/admin")
	}

	return c.Render(http.StatusOK, "admin/forgot-password", d)
}

// SendPasswordReset checks if the given email address belongs to an existing
// user and if so, sends a password reset link to that email
func SendPasswordReset(c echo.Context) error {
	pr := new(model.PasswordReset)
	pr.Email = c.FormValue("Email")

	if pr.Email == "" {
		d := struct {
			SiteURL string
			Message template.HTML
		}{
			config.SiteURL,
			`<div class="errorMessage">
			    <p>
			        Please enter a valid email address.
			    </p>
			</div>`,
		}
		return c.Render(http.StatusBadRequest, "admin/forgot-password", d)
	}

	emailExist, username := model.EmailExist(pr.Email)

	if !emailExist {
		d := struct {
			SiteURL string
			Message template.HTML
		}{
			config.SiteURL,
			`<div class="errorMessage">
			    <p>
			        The entered email does not exist.
			    </p>
			</div>`,
		}
		return c.Render(http.StatusBadRequest, "admin/forgot-password", d)
	}

	pr.ID = bson.NewObjectId()
	pr.Username = username
	pr.ExpiryDate = time.Now().Add(time.Hour * 24 * 5)

	linkCreated := model.CreatePasswordResetLink(pr)

	if !linkCreated {
		d := struct {
			SiteURL string
			Message template.HTML
		}{
			config.SiteURL,
			`<div class="errorMessage">
			    <p>
			        Error creating password reset link.<br>
                    Please try again.
			    </p>
			</div>`,
		}
		return c.Render(http.StatusBadRequest, "admin/forgot-password", d)
	}

	link := config.SiteURL + "/pwdrstlnk/" + pr.ID.Hex()

	mail.SendPasswordResetLink(link, pr.Email)

	return c.Render(http.StatusFound, "admin/password-reset-sent", nil)
}

// EnterNewPassword shows the new password page
func EnterNewPassword(c echo.Context) error {
	id := c.Param("id")

	pr := model.GetPasswordReset(id)

	if pr.ExpiryDate.Sub(time.Now()) < 0 {
		d := struct {
			SiteURL string
			Expired bool
			ID      bson.ObjectId
			Message template.HTML
		}{
			config.SiteURL,
			true,
			"",
			"",
		}
		return c.Render(http.StatusBadRequest, "admin/new-password", d)
	}

	d := struct {
		SiteURL string
		Expired bool
		ID      bson.ObjectId
		Message template.HTML
	}{
		config.SiteURL,
		false,
		pr.ID,
		"",
	}

	return c.Render(http.StatusOK, "admin/new-password", d)

}

// CreateNewPassword creates the new password for the user associated with the
// password reset ID
func CreateNewPassword(c echo.Context) error {
	id := c.Param("id")
	password := c.FormValue("Password")

	pr := model.GetPasswordReset(id)

	if pr.ExpiryDate.Sub(time.Now()) < 0 {
		d := struct {
			SiteURL string
			Expired bool
			ID      bson.ObjectId
			Message template.HTML
		}{
			config.SiteURL,
			true,
			"",
			"",
		}
		return c.Render(http.StatusBadRequest, "admin/new-password", d)
	}

	if password == "" {
		d := struct {
			SiteURL string
			Expired bool
			ID      bson.ObjectId
			Message template.HTML
		}{
			config.SiteURL,
			false,
			pr.ID,
			`<div class="errorMessage">
			    <p>
			        Please enter a password.
			    </p>
			</div>`,
		}
		return c.Render(http.StatusBadRequest, "admin/new-password", d)
	}

	hashedPassword, err := hash.String(password)
	if err != nil {
		log.Panic(err)
	}

	model.SaveNewPassword(pr.Username, hashedPassword)
	model.RemovePasswordReset(pr.Username)

	return c.Render(http.StatusOK, "admin/password-changed", nil)
}
