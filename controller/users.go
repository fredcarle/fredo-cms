package controller

import (
	"log"
	"net/http"
	"regexp"

	"gopkg.in/mgo.v2/bson"

	"github.com/labstack/echo"
	"gitlab.com/fredcarle/fredo-cms/model"
	"gitlab.com/fredcarle/fredo-cms/utils/config"
	"gitlab.com/fredcarle/fredo-cms/utils/hash"
)

// ShowUsers shows the list of editable users
func ShowUsers(c echo.Context) error {
	d := new(model.UsersEdit)
	d.Users = model.GetAllUsers()
	d.SiteURL = config.SiteURL
	return c.Render(http.StatusOK, "admin/users", d)
}

// NewUser loads the user creation page
func NewUser(c echo.Context) error {
	d := new(model.UserEdit)
	d.User.ID = bson.NewObjectId()
	d.User.Role = model.UserRole
	d.SiteURL = config.SiteURL
	d.New = true
	return c.Render(http.StatusOK, "admin/edituser", d)
}

// EditUser show the post editing form
func EditUser(c echo.Context) error {
	d := new(model.UserEdit)
	d.User.ID = bson.ObjectIdHex(c.Param("id"))
	d.User.GetUser()
	d.SiteURL = config.SiteURL
	d.New = false
	return c.Render(http.StatusOK, "admin/edituser", d)
}

// SaveUser saves the user in the database and returns the user to the
// main admin page
func SaveUser(c echo.Context) error {
	u := new(model.User)
	u.Username = c.FormValue("Username")
	u.Firstname = c.FormValue("Firstname")
	u.Lastname = c.FormValue("Lastname")
	u.Email = c.FormValue("Email")
	u.ID = bson.ObjectIdHex(c.Param("id"))
	u.Role = c.FormValue("Role")
	if u.Username == "" || u.Email == "" || u.ID == "" {
		d := new(model.UserEdit)
		d.SiteURL = config.SiteURL
		d.User = *u

		return c.Render(http.StatusBadRequest, "admin/edituser", d)
	}

	del := c.FormValue("Delete")
	matched, _ := regexp.MatchString("^(?i)delete$", del)
	switch matched {
	case true:
		model.DeleteUser(u.Username)
		return c.Redirect(http.StatusFound, "/admin/users")
	}

	if c.FormValue("Password") != "" {
		var err error
		u.Password, err = hash.String(c.FormValue("Password"))
		if err != nil {
			log.Panic(err)
		}
	}

	u.SaveUser()

	return c.Redirect(http.StatusFound, "/admin/users")
}
