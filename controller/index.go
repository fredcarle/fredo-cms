package controller

import (
	"net/http"

	"gitlab.com/fredcarle/fredo-cms/utils/config"

	"github.com/labstack/echo"
)

// Index shows the website main page with all the most recent posts
func Index(c echo.Context) error {
	d := struct {
		SiteURL string
	}{config.SiteURL}
	return c.Render(http.StatusOK, config.TemplateName+"/index", d)
}
