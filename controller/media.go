package controller

import (
	"io"
	"mime/multipart"
	"os"
	"strconv"
	"time"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/fredcarle/fredo-cms/model"
	"gitlab.com/fredcarle/fredo-cms/utils/config"
)

// SaveImagesToDisk saves the uploaded images to disk
func SaveImagesToDisk(form *multipart.Form) ([]model.Image, error) {

	images := form.File["Images"]

	var imageList []model.Image

	for _, image := range images {
		// source
		src, err := image.Open()
		if err != nil {
			return nil, err
		}
		defer src.Close()

		var month string

		if int(time.Now().Month()) < 10 {
			month = "0" + strconv.Itoa(int(time.Now().Month()))
		} else {
			month = strconv.Itoa(int(time.Now().Month()))
		}

		path := config.ImagesPath + strconv.Itoa(time.Now().Year()) + "/" + month
		imgPath := path + "/" + image.Filename

		os.MkdirAll(path, 0777)

		// destination
		dst, err := os.Create(imgPath)
		if err != nil {
			return nil, err
		}
		defer dst.Close()

		// Copy
		if _, err = io.Copy(dst, src); err != nil {
			return nil, err
		}

		img := new(model.Image)

		img.ID = bson.NewObjectId()
		img.URL = config.SiteURL + "/" + imgPath
		img.SaveImage()

		imageList = append(imageList, *img)
	}
	return imageList, nil
}
