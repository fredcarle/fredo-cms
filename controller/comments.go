package controller

import (
	"net/http"
	"regexp"
	"time"

	"github.com/labstack/echo"
	"gitlab.com/fredcarle/fredo-cms/model"
	"gitlab.com/fredcarle/fredo-cms/utils/config"
	"gitlab.com/fredcarle/fredo-cms/utils/session"
	"gopkg.in/mgo.v2/bson"
)

// ShowComments shows the list of comments
func ShowComments(c echo.Context) error {
	d := struct {
		SiteURL  string
		Comments []model.Comment
	}{
		config.SiteURL,
		model.GetComments(""),
	}
	return c.Render(http.StatusOK, "admin/comments", d)
}

// EditComment show the comment editing form
func EditComment(c echo.Context) error {
	d := struct {
		SiteURL string
		Comment model.Comment
	}{
		config.SiteURL,
		model.GetOneComment(bson.ObjectIdHex(c.Param("id"))),
	}
	return c.Render(http.StatusOK, "admin/editcomment", d)
}

// SaveComment saves the comment in the database and returns the user to the
// main admin page
func SaveComment(c echo.Context) error {

	comment := new(model.Comment)
	comment.ID = bson.ObjectIdHex(c.Param("id"))
	// comment.Author.Username = c.FormValue("AuthorUsername")
	comment.Status = c.FormValue("Status")
	// model.GetAuthor(&comment.Author)
	comment.Content = c.FormValue("Content")
	del := c.FormValue("Delete")
	matched, _ := regexp.MatchString("^(?i)delete$", del)
	switch matched {
	case true:
		comment := model.GetOneComment(bson.ObjectIdHex(c.Param("id")))
		comment.DeleteComment()
		return c.Redirect(http.StatusFound, "/admin/comments")
	}

	comment.UpdateComment()

	return c.Redirect(http.StatusFound, "/admin/comments")
}

// PostComment saves the post in the database and returns the user to the
// appropriate post page
func PostComment(c echo.Context) error {

	comment := new(model.Comment)
	comment.ID = bson.NewObjectId()
	comment.Author.Username = session.GetAuthorUsername(c)
	if comment.Author.Username == "" {
		c.SetCookie(session.CreateRedirectionCookie(config.SiteURL + "/post/" + c.Param("slug")))
		return c.Redirect(http.StatusFound, "/login")
	}
	model.GetAuthor(&comment.Author)
	comment.Content = c.FormValue("Content")
	comment.Date = time.Now()
	comment.On = c.Param("slug")
	comment.AuthorIP = session.RealIP(c)
	comment.Status = "approved"
	switch c.Param("reply") {
	case "1":
		comment.IsReply = true
		comment.ReplyTo = bson.ObjectIdHex(c.Param("id"))
	default:
		comment.IsReply = false
	}

	// if comment.Content == "" {
	// 	d := new(model.SinglePost)
	// 	d.SiteURL = config.SiteURL
	// 	d.Comment = *comment
	//
	// 	return c.Render(http.StatusBadRequest, "admin/editcomment", d)
	// }

	comment.SaveComment()

	return c.Redirect(http.StatusFound, "/post/"+comment.On)
}

// DeleteComment saves the post in the database and returns the user to the
// appropriate post page
func DeleteComment(c echo.Context) error {
	comment := model.GetOneComment(bson.ObjectIdHex(c.Param("id")))
	comment.DeleteComment()
	return c.Redirect(http.StatusFound, "/admin/comments")
}
