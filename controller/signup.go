package controller

import (
	"log"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/fredcarle/fredo-cms/model"
	"gitlab.com/fredcarle/fredo-cms/utils/config"
	"gitlab.com/fredcarle/fredo-cms/utils/hash"
	"gitlab.com/fredcarle/fredo-cms/utils/session"
	"gopkg.in/mgo.v2/bson"
)

// SignUp sends the user to the registration form with a preassigned user ID
func SignUp(c echo.Context) error {

	d := new(model.UserEdit)
	d.User.ID = bson.NewObjectId()
	d.SiteURL = config.SiteURL

	return c.Render(http.StatusOK, "admin/signup", d)
}

// SignUpUser checks for valid field and save to user to database if it passes.
// Otherwise, it returns the user to the register page
func SignUpUser(c echo.Context) error {

	u := new(model.User)
	u.Username = c.FormValue("Username")
	u.Firstname = c.FormValue("Firstname")
	u.Lastname = c.FormValue("Lastname")
	u.Email = c.FormValue("Email")
	u.ID = bson.ObjectIdHex(c.Param("id"))
	if model.IsFirstUser() {
		u.Role = model.AdminRole
	} else {
		u.Role = model.UserRole
	}

	if u.Username == "" || u.Email == "" || u.ID == "" || c.FormValue("Password") == "" {
		d := new(model.UserEdit)
		d.SiteURL = config.SiteURL
		d.User = *u

		return c.Render(http.StatusBadRequest, "admin/signup", d)
	}

	var err error
	u.Password, err = hash.String(c.FormValue("Password"))
	if err != nil {
		log.Panic(err)
	}

	u.SaveUser()

	tokenString := session.CreateToken(u)

	c.SetCookie(session.CreateCookie(tokenString))

	d := struct {
		SiteURL string
	}{config.SiteURL}

	return c.Render(http.StatusOK, "admin/registrationconfirmation", d)
}
