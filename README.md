# Fredo

#### Fredo is a Content Management System built with the HTTP server framework [echo](https://echo.labstack.com/).

*Please note that this is a work in progress and some feature may not be working properly. Also, the default site template is yet to be developed and the admin interface template is still primitive (mobile first).*

## Installation

First, you need to install Fredo

```sh
$ go get -u gitlab.com/fredcarle/fredo-cms
```

### MongoDB
Then, make sure you have a version of MongoDB installed. Fredo was built with version 3.2 so it is suggested that you have a version 3.2 or later installed. Please visit the [MongoDB website](https://docs.mongodb.com/manual/installation/) for installation instruction.

A database named "fredo" must be created along with a user of the same name with Read/Write privilege. Once the user is created, you must add the user password to the .config.json file.

It is recommended that you run `mongod` with the `--auth` option.

```sh
$ mongod --auth
```

### Mail Server
If you want sendmail functionality, you must enter your email server and user credentials in the .config.json file.

### JWT
You will need a public and private RSA key for the JWT functionality. You can use openssl to do so.

```sh
$ openssl genpkey -algorithm RSA -out .rsa.pem -pkeyopt rsa_keygen_bits:2048
```

```sh
$ openssl rsa -pubout -in .rsa.pem -out .rsa_pub.pem
```

Or you can ask Fredo to do it for you with the `--create-rsa` option flag.

```sh
$ fredo-cms --create-rsa
```
