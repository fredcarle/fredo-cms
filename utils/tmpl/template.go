package tmpl

import (
	"html/template"
	"io"
	"log"
	"path/filepath"
	"strings"

	"gopkg.in/mgo.v2/bson"

	"github.com/labstack/echo"
	"gitlab.com/fredcarle/fredo-cms/model"
	"gitlab.com/fredcarle/fredo-cms/utils/config"
	"gitlab.com/fredcarle/fredo-cms/utils/markdown"
)

// Template is the structure used to old the template names
type Template struct {
	Templates *template.Template
}

// Render renders the template files
func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.Templates.ExecuteTemplate(w, name, data)
}

// Excerpt trims the content to only the content before <!-more->
func Excerpt(s string) string {
	excerpt := strings.Split(s, "<!--more-->")
	return excerpt[0]
}

// Delimit takes an array and return a string with the array elements
// delimited by the given delimiter
func Delimit(array []string, delimiter string) string {
	return strings.Join(array, delimiter)
}

// GetReplies queries for a list of comment replies
func GetReplies(replies []bson.ObjectId) []model.Comment {
	return model.GetReplies(replies)
}

// GetPosts gets the complete list of posts
func GetPosts() []model.Post {
	return model.GetAllPost()
}

// GetHomePage gets the page titled "home" from the database
func GetHomePage() model.Page {
	return model.GetHomePage()
}

// GetTemplates gets the templates for the admin panel and the public site
func GetTemplates() *Template {
	tAdmin, err := filepath.Glob("views/admin/*html")
	if err != nil {
		log.Fatal(err)
	}

	tPublic, err := filepath.Glob("views/templates/" + config.TemplateName + "/*.html")
	if err != nil {
		log.Fatal(err)
	}

	files := append(tAdmin, tPublic...)

	t := &Template{
		// Maybe there's a better way of doing this?
		Templates: template.Must(template.New("").Funcs(template.FuncMap{"ToMarkdown": markdown.ToMarkdown,
			"Excerpt": Excerpt, "Delimit": Delimit, "GetReplies": GetReplies, "GetPosts": GetPosts, "GetHomePage": GetHomePage}).ParseFiles(files...)),
	}

	return t
}
