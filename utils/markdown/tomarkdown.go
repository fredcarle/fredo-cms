package markdown

import (
	"html/template"

	"github.com/russross/blackfriday"
)

// ToMarkdown converts the markdown formated content string to a template.HTML
func ToMarkdown(s string) template.HTML {
	return template.HTML(blackfriday.MarkdownCommon([]byte(s)))
}
