package config

import (
	"crypto/rsa"
	"encoding/json"
	"io/ioutil"
	"log"
)

var (
	// SiteURL hold the main URL/domain name for the site
	SiteURL string
	// DB holds the mongodb configuration
	DB MongoDB
	// Email holds the email credetials
	Email EmailCred
	// SignKey hold the binary private key
	SignKey *rsa.PrivateKey
	// VerifyKey hold the binary public key
	VerifyKey *rsa.PublicKey
	// Keys hold the paths to the RSA keys
	Keys RSA
	// TemplateName is the variable holding the template name
	TemplateName string
)

const (
	// ImagesPath is the location on the server where the uploaded images
	// will be stored
	ImagesPath = "media/images/"
)

// RSA hold the path to the public and private RSA keys
type RSA struct {
	PrivKeyPath   string
	PubKeyPath    string
	SigningMethod string
}

// MongoDB is the struct containing the configuration elements
type MongoDB struct {
	URL      string
	Name     string
	Username string
	Password string
}

// EmailCred is the struct containing the email credetials for the app to
// send mail
type EmailCred struct {
	Email      string
	Password   string
	MailServer string
	Port       string
}

// siteConfig contains all the website configs
type configuration struct {
	URL      string
	Template string
	DB       MongoDB
	Session  RSA
	Email    EmailCred
}

// Load imports the config.json confiugation file
func Load(file string) {
	jsonFile, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)

	}
	config := new(configuration)
	err = json.Unmarshal(jsonFile, &config)
	if err != nil {
		log.Fatal(err)
	}

	DB = config.DB
	TemplateName = config.Template
	SiteURL = config.URL
	Keys = config.Session
	Email = config.Email

}
