package session

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"gitlab.com/fredcarle/fredo-cms/model"
	"gitlab.com/fredcarle/fredo-cms/utils/config"
)

// GetRSA grabs the public and private RSA keys from their respective files
func GetRSA() {

	signBytes, _ := ioutil.ReadFile(config.Keys.PrivKeyPath)

	verifyBytes, _ := ioutil.ReadFile(config.Keys.PubKeyPath)

	var err error

	// Get the private key to create JWT tokens
	privBlock, _ := pem.Decode(signBytes)
	config.SignKey, err = x509.ParsePKCS1PrivateKey(privBlock.Bytes)
	if err != nil {
		log.Fatal("[KEY PARSING] Error parsing RSA private key:", err)
	}

	// Get the public key to use with the JWT middleware
	pubBlock, _ := pem.Decode(verifyBytes)
	pub, err := x509.ParsePKIXPublicKey(pubBlock.Bytes)
	config.VerifyKey = pub.(*rsa.PublicKey)
	if err != nil {
		log.Fatal("[KEY PARSING] Error parsing RSA public key:", err)
	}

}

// CheckToken is a middleware that grabs the token string from
// the cookies and passes it to the request header for the jwt
// middleware to parse
func CheckToken(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		cookie, err := c.Cookie("token")
		if err != nil {
			return next(c)
		}

		c.Request().Header().Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", cookie.Value()))

		return next(c)
	}
}

// CreateToken creates a JWT token for user sesssions
func CreateToken(user *model.User) string {
	token := jwt.New(jwt.SigningMethodRS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = user.Username
	claims["role"] = user.Role
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()

	tokenString, err := token.SignedString(config.SignKey)
	// tokenString, err := token.SignedString([]byte("secret"))

	if err != nil {
		log.Fatal("[CREATETOKEN] Error creating signed token: ", err)
	}

	return tokenString
}

// ValidateToken validates the token
func ValidateToken(c echo.Context) bool {
	tokenString := readCookie(c)

	if tokenString == "" {
		return false
	}
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		// if token.Method.Alg() != "HS256" {
		if token.Method.Alg() != config.Keys.SigningMethod {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		// return []byte("secret"), nil
		return config.VerifyKey, nil
	})

	if err == nil && token.Valid {
		return true
	}
	return false
}

// CheckUserRole checks the user role to determine site access privilege
func CheckUserRole(next echo.HandlerFunc) echo.HandlerFunc {

	return func(c echo.Context) error {

		token := c.Get("user").(*jwt.Token)
		claims := token.Claims.(jwt.MapClaims)
		u := new(model.User)
		u.Username = claims["username"].(string)
		u.Role = claims["role"].(string)
		switch u.Role {
		case "admin":

			return next(c)
		}
		return c.Redirect(http.StatusFound, "/")
	}
}

// CheckAuthorUsername checks the author's username
func CheckAuthorUsername(c echo.Context) string {
	token := c.Get("user").(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	if token != nil {
		return claims["username"].(string)
	}
	return ""

}

// GetAuthorUsername checks the author's username from the cookie
func GetAuthorUsername(c echo.Context) string {
	tokenString := readCookie(c)

	if tokenString == "" {
		return ""
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if token.Method.Alg() != config.Keys.SigningMethod {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return config.VerifyKey, nil
	})

	if err == nil && token != nil {
		return token.Claims.(jwt.MapClaims)["username"].(string)
	}
	return ""
}

// CreateCookie creates a cookie
func CreateCookie(tokenString string) *echo.Cookie {
	cookie := new(echo.Cookie)
	cookie.SetName("token")
	cookie.SetPath("/")
	cookie.SetValue(tokenString)
	cookie.SetExpires(time.Now().Add(24 * time.Hour))
	return cookie
}

func readCookie(c echo.Context) string {
	cookie, err := c.Cookie("token")
	if err != nil {
		return ""
	}
	return cookie.Value()
}

// CreateRedirectionCookie saves the link to be redirected to after login
func CreateRedirectionCookie(url string) *echo.Cookie {
	cookie := new(echo.Cookie)
	cookie.SetName("redirect")
	cookie.SetPath("/")
	cookie.SetValue(url)
	cookie.SetExpires(time.Now().Add(1 * time.Hour))
	return cookie
}

// ReadRedirectionCookie checks if the user came from somewhere other than
// direct acccess to the login page
func ReadRedirectionCookie(c echo.Context) string {
	cookie, err := c.Cookie("redirect")
	if err != nil {
		return ""
	}
	return cookie.Value()
}

// InvalidateCookie simply sets the token string and
// the expire date in a way to invalidate it
func InvalidateCookie(s string) *echo.Cookie {
	cookie := new(echo.Cookie)
	cookie.SetName("token")
	cookie.SetPath("/")
	cookie.SetValue(s)
	cookie.SetExpires(time.Now().Add(time.Hour * -10))
	return cookie
}

// RealIP sends back the user's ip address.
func RealIP(c echo.Context) string {
	ra := c.Request().RemoteAddress()
	if ip := c.Request().Header().Get(echo.HeaderXForwardedFor); ip != "" {
		ra = ip
	} else if ip := c.Request().Header().Get(echo.HeaderXRealIP); ip != "" {
		ra = ip
	} else {
		ra, _, _ = net.SplitHostPort(ra)
	}
	return ra
}

// GenerateRSAKeys generates the public and private keys for the JWT functionality
func GenerateRSAKeys() {
	privKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		log.Fatal("[KEYGEN] Error creating RSA private key:", err)
	}

	err = privKey.Validate()
	if err != nil {
		log.Fatal("[KEYGEN] Error validating RSA private key:", err)
	}

	privBytes := x509.MarshalPKCS1PrivateKey(privKey)

	privBlock := pem.Block{
		Type:    "RSA PRIVATE KEY",
		Headers: nil,
		Bytes:   privBytes,
	}

	rsaPrivFile, err := os.OpenFile(config.Keys.PrivKeyPath, os.O_RDWR|os.O_CREATE, 0666)
	defer rsaPrivFile.Close()
	if err != nil {
		log.Fatal("[RSAFILE] Error opening privKey file:", err)
	}

	err = pem.Encode(rsaPrivFile, &privBlock)
	if err != nil {
		log.Fatal("[RSAFILE] Error saving RSA private key to file:", err)
	}

	pubKey := privKey.PublicKey

	pubBytes, err := x509.MarshalPKIXPublicKey(&pubKey)
	if err != nil {
		log.Fatal("[KEYGEN] Error marshalling RSA public key:", err)
	}

	pubBlock := pem.Block{
		Type:    "PUBLIC KEY",
		Headers: nil,
		Bytes:   pubBytes,
	}

	rsaPubFile, err := os.OpenFile(config.Keys.PubKeyPath, os.O_RDWR|os.O_CREATE, 0666)
	defer rsaPubFile.Close()
	if err != nil {
		log.Fatal("[RSAFILE] Error opening pubKey file:", err)
	}

	err = pem.Encode(rsaPubFile, &pubBlock)
	if err != nil {
		log.Fatal("[RSAFILE] Error saving RSA public key to file:", err)
	}

}
