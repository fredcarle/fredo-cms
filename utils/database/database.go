package database

import (
	"log"

	"gitlab.com/fredcarle/fredo-cms/utils/config"

	"gopkg.in/mgo.v2"
)

const (
	// PostCollection is the name of the Mongo collection that holds posts
	PostCollection = "posts"
	// UserCollection is the name of the Mongo collection that holds users
	UserCollection = "users"
	// PageCollection is the name of the Mongo collection that holds pages
	PageCollection = "pages"
	// ImageCollection is the name of the Mongo collection that holds images
	ImageCollection = "images"
	// CommentCollection is the name of the Mongo collection that holds comments
	CommentCollection = "comments"
	// PasswordReset is the name of the Mongo collection that holds the
	// temporary password reset ID's associated with a given user
	PasswordReset = "password_reset"
)

var (
	// Mongo is the holder of the main mongodb session
	Mongo *mgo.Session
)

// InitMongoCollection initializes collections
func InitMongoCollection(coll, key string, unique bool) {
	collection := Mongo.DB(config.DB.Name).C(coll)

	if key != "" {
		index := mgo.Index{
			Key:        []string{key},
			Unique:     unique,
			DropDups:   true,
			Background: true,
			Sparse:     true,
		}

		err := collection.EnsureIndex(index)
		if err != nil {
			log.Panic(err)
		}
	}
}

// NewMongoSession creates the main mongodb session
func NewMongoSession() {
	var err error
	dialInfo := new(mgo.DialInfo)
	dialInfo.Addrs = []string{config.DB.URL}
	dialInfo.Username = config.DB.Username
	dialInfo.Password = config.DB.Password
	dialInfo.Database = config.DB.Name

	Mongo, err = mgo.DialWithInfo(dialInfo)
	if err != nil {
		log.Panic(err)
	}

	Mongo.SetMode(mgo.Monotonic, true)

	InitMongoCollection(PostCollection, "slug", true)
	InitMongoCollection(PageCollection, "slug", true)
	InitMongoCollection(UserCollection, "username", true)
	InitMongoCollection(PasswordReset, "", false)
}
