package url

import (
	"log"
	"regexp"
	"strings"
)

// PrettyURL transforms the post title to a pretty URL
func PrettyURL(title string) string {
	// Removing the every special character and spaces except "'"
	// from the title and replacing with "-"
	reg, err := regexp.Compile("[^A-Za-z0-9']+")
	if err != nil {
		log.Panic(err)
	}
	prettyurl := reg.ReplaceAllString(title, "-")

	// Removing "'" from the title
	reg, err = regexp.Compile("[^-A-Za-z0-9]+")
	if err != nil {
		log.Panic(err)
	}
	prettyurl = reg.ReplaceAllString(prettyurl, "")

	//Trimming the pre and post "-" if any
	prettyurl = strings.ToLower(strings.Trim(prettyurl, "-"))

	return prettyurl
}
