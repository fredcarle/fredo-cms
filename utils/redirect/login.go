package redirect

import (
	"errors"
	"net/http"

	"github.com/labstack/echo"
)

// Login redirect the user to the login page if not logged in and
// trying to access the admin panel.
func Login(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		jwtError := errors.New("empty jwt in cookie")

		if err := next(c); err != nil {
			if err.Error() == jwtError.Error() {
				// fmt.Println("got 400")
				return c.Redirect(http.StatusFound, "/login")
			}
		}

		return nil
	}
}
