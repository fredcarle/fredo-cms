package hash

import "golang.org/x/crypto/bcrypt"

// String returns a hashed password string using bcrypt.
func String(password string) ([]byte, error) {
	key, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err == nil {
		return key, nil
	}

	return nil, err
}

// Match return true if the password string matches the hash value for the user.
func Match(hashedPassword []byte, password string) bool {
	err := bcrypt.CompareHashAndPassword(hashedPassword, []byte(password))
	if err == nil {
		return true
	}

	return false
}
