package mail

import (
	"bytes"
	"html/template"
	"log"
	"net/smtp"

	"gitlab.com/fredcarle/fredo-cms/utils/config"
)

//EmailTemplateData is the struct containing email data to insert into the
// email template
type EmailTemplateData struct {
	From    string
	To      string
	Subject string
	Body    string
}

const emailTemplate = `From: {{ .From }}
To: {{ .To }}
Subject: {{ .Subject }}

{{ .Body }}

Sincerely,

fredo
`

// SendPasswordResetLink send the password reset link to the user
func SendPasswordResetLink(passResetURL, userEmail string) {
	var (
		err error
		doc bytes.Buffer
	)

	auth := smtp.PlainAuth("", config.Email.Email, config.Email.Password, config.Email.MailServer)

	context := new(EmailTemplateData)

	context.From = config.Email.Email
	context.To = userEmail
	context.Subject = "Paassword reset link"
	context.Body = passResetURL

	t := template.New("emailTemplate")
	if t, err = t.Parse(emailTemplate); err != nil {
		log.Print("error trying to parse email template: ", err)
	}

	if err = t.Execute(&doc, context); err != nil {
		log.Print("error trying to execute mail template: ", err)
	}

	to := []string{userEmail}
	err = smtp.SendMail(config.Email.MailServer+":"+config.Email.Port, auth, config.Email.Email, to, doc.Bytes())
	if err != nil {
		log.Printf("[SENDMAIL] Error sending password reset link: %v", err)
	}
}
