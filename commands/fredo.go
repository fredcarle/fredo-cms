package commands

import (
	"flag"

	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/standard"
	"github.com/labstack/echo/middleware"
	"gitlab.com/fredcarle/fredo-cms/controller"
	"gitlab.com/fredcarle/fredo-cms/utils/config"
	"gitlab.com/fredcarle/fredo-cms/utils/database"
	"gitlab.com/fredcarle/fredo-cms/utils/redirect"
	"gitlab.com/fredcarle/fredo-cms/utils/session"
	"gitlab.com/fredcarle/fredo-cms/utils/tmpl"
)

var (
	createRSAFlag = flag.Bool("create-rsa", false, "Ask Fredo to generate new RSA key pair")
)

// StartFredo launches the application with routers, middlewares and
// database as specified.
func StartFredo() {
	flag.Parse()
	config.Load(".config.json")

	if *createRSAFlag {
		session.GenerateRSAKeys()
		return
	}

	session.GetRSA()

	database.NewMongoSession()
	defer database.Mongo.Close()

	t := tmpl.GetTemplates()

	e := echo.New()

	e.SetRenderer(t)
	e.SetDebug(true)

	e.Pre(middleware.RemoveTrailingSlash())

	e.Static("/tmpl", "views/templates/"+config.TemplateName+"/static")
	e.Static("/static", "views/admin/static")
	e.Static("/media/images", "media/images")

	// logFile, err := os.OpenFile("log.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777)
	// if err != nil {
	// 	log.Panic(err)
	// }

	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "time: ${time_rfc3339}, remote_ip: ${remote_ip}, " +
			"method: ${method}, uri: ${uri}, status: ${status}, latency: ${latency}, " +
			"latency_human: ${latency_human}, bytes_in: ${bytes_in}, " +
			"bytes_out: ${bytes_out}}" + "\n",
		// Output: logFile,
	}))

	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{config.SiteURL},
		AllowMethods: []string{echo.GET, echo.PUT, echo.POST, echo.DELETE},
	}))
	e.Use(middleware.Static("media/images"))
	e.Use(middleware.Secure())
	e.Use(session.CheckToken)
	e.GET("/", controller.Index)
	e.GET("/:page", controller.SinglePage)
	e.GET("/post/:slug", controller.SinglePost)
	e.GET("/login", controller.Login)
	e.POST("/userlogin", controller.LoginUser)
	e.GET("/signup", controller.SignUp)
	e.POST("/signup/:id", controller.SignUpUser)
	e.GET("/logout", controller.Logout)
	e.GET("/forgot_password", controller.ForgotPassword)
	e.POST("/send_password_reset", controller.SendPasswordReset)
	e.GET("/pwdrstlnk/:id", controller.EnterNewPassword)
	e.POST("/create_new_password/:id", controller.CreateNewPassword)
	e.POST("/post/:slug/post_comment/:reply", controller.PostComment)
	e.POST("/post/:slug/post_comment/:reply/:id", controller.PostComment)

	a := e.Group("/admin")

	a.Use(redirect.Login)

	a.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey:    config.VerifyKey,
		TokenLookup:   "cookie:token",
		SigningMethod: config.Keys.SigningMethod,
	}))

	a.Use(session.CheckUserRole)

	a.GET("", controller.Admin)

	a.GET("/pages", controller.ShowPages)
	a.GET("/newpage", controller.NewPage)
	a.GET("/editpage/:id", controller.EditPage)
	a.POST("/savepage/:id", controller.SavePage)

	a.GET("/posts", controller.ShowPosts)
	a.GET("/newpost", controller.NewPost)
	a.GET("/editpost/:id", controller.EditPost)
	a.POST("/savepost/:id", controller.SavePost)

	a.GET("/comments", controller.ShowComments)
	a.GET("/deletecomment/:id", controller.DeleteComment)
	a.GET("/editcomment/:id", controller.EditComment)
	a.POST("/savecomment/:id", controller.SaveComment)

	a.GET("/users", controller.ShowUsers)
	a.GET("/newuser", controller.NewUser)
	a.GET("/edituser/:id", controller.EditUser)
	a.POST("/saveuser/:id", controller.SaveUser)

	e.Run(standard.New(":1818"))

	// e.Run(standard.WithConfig(engine.Config{
	// 	Address:     ":1818",
	// 	TLSCertFile: "cert.pem",
	// 	TLSKeyFile:  "key.pem",
	// }))
}
