package model

import (
	"log"
	"time"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/fredcarle/fredo-cms/utils/config"
	"gitlab.com/fredcarle/fredo-cms/utils/database"
)

// Post is the struct containing all the relevent details of a given post
type Post struct {
	ID            bson.ObjectId `bson:"_id"`
	Status        string
	Slug          string
	Title         string
	Author        PostAuthor
	Date          time.Time
	Categories    []string
	Tags          []string
	Content       string
	Images        []Image
	TotalComments int
	Comments      []Comment
}

// AllPosts is the struct containing the relevent details for the home page
type AllPosts struct {
	SiteURL string
	Posts   []Post
}

// SinglePost is the struct containing the relevent details for a single post
type SinglePost struct {
	SiteURL string
	Post    Post
}

// PostEdit is the struct containing the relevent details for post editing
type PostEdit struct {
	SiteURL string
	Post    Post
	New     bool
}

// SavePost save the post struct in the database
func (post *Post) SavePost() {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.PostCollection)

	_, err := col.Upsert(bson.M{"_id": post.ID}, post)
	if err != nil {
		log.Panic(err)
	}
	UpdateUserPostList(post.Author.Username, post.ID)

}

// GetSinglePost gets the relevent post using the post ID
func (post *Post) GetSinglePost() {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.PostCollection)

	if post.Slug == "" {
		err := col.Find(bson.M{"_id": post.ID}).One(post)
		if err != nil {
			log.Panic(err)
		}
	} else {
		err := col.Find(bson.M{"slug": post.Slug}).One(post)
		if err != nil {
			log.Panic(err)
		}
	}

}

// GetAllPost returns all post from the database for main site
func GetAllPost() []Post {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.PostCollection)

	result := []Post{}

	err := col.Find(bson.M{"status": "publish"}).Select(bson.M{"images": 0}).Sort("-date").All(&result)
	if err != nil {
		log.Panic(err)
	}

	return result
}

// GetAllPostsForAdmin returns all post from the database for admin panel
func GetAllPostsForAdmin() []Post {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.PostCollection)

	result := []Post{}

	err := col.Find(nil).Select(bson.M{"content": 0, "images": 0}).Sort("-date").All(&result)
	if err != nil {
		log.Panic(err)
	}

	return result
}

// PostCount return the total number of posts
func PostCount() int {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.PostCollection)

	count, err := col.Find(nil).Count()
	if err != nil {
		log.Panic(err)
	}

	return count

}

// DeletePost deletes the given user from the database
func DeletePost(id bson.ObjectId) {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.PostCollection)

	err := col.Remove(bson.M{"_id": id})
	if err != nil {
		log.Panic(err)
	}
}
