package model

import (
	"log"

	"gitlab.com/fredcarle/fredo-cms/utils/config"
	"gitlab.com/fredcarle/fredo-cms/utils/database"
	"gopkg.in/mgo.v2/bson"
)

// Image is a struct containing the relevant image details
type Image struct {
	ID      bson.ObjectId `bson:"_id"`
	Alt     string
	URL     string
	Caption string
}

// SaveImage saves the image struct in the database
func (image *Image) SaveImage() {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.ImageCollection)

	_, err := col.Upsert(bson.M{"_id": image.ID}, image)
	if err != nil {
		log.Panic(err)
	}
}
