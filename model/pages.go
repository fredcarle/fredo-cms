package model

import (
	"log"
	"time"

	"gitlab.com/fredcarle/fredo-cms/utils/config"
	"gitlab.com/fredcarle/fredo-cms/utils/database"

	"gopkg.in/mgo.v2/bson"
)

// Page is the struct containing all the relevent details of a given page
type Page struct {
	ID      bson.ObjectId `bson:"_id"`
	Status  string
	Slug    string
	Title   string
	Author  PostAuthor
	Date    time.Time
	Content string
	Images  []Image
}

// AllPages is the struct containing the relevent details for the home page
type AllPages struct {
	SiteURL string
	Pages   []Page
}

// SinglePage is the struct containing the relevent details for a single page
type SinglePage struct {
	SiteURL string
	Page    Page
}

// PageEdit is the struct containing the relevent details for page editing
type PageEdit struct {
	SiteURL string
	Page    Page
	New     bool
}

// SavePage save the page struct in the database
func (page *Page) SavePage() {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.PageCollection)

	_, err := col.Upsert(bson.M{"_id": page.ID}, page)
	if err != nil {
		log.Panic(err)
	}
}

// GetSinglePage gets the relevent page using the page ID
func (page *Page) GetSinglePage() {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.PageCollection)

	if page.Slug == "" {
		err := col.Find(bson.M{"_id": page.ID}).One(page)
		if err != nil {
			log.Panic(err)
		}
	} else {
		err := col.Find(bson.M{"slug": page.Slug}).One(page)
		if err != nil {
			log.Panic(err)
		}
	}

}

// GetHomePage gets the relevent page using the page ID
func GetHomePage() Page {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.PageCollection)

	page := new(Page)
	err := col.Find(bson.M{"slug": "home"}).One(page)
	if err != nil {
		log.Panic(err)
	}
	return *page
}

// GetAllPages returns all page from the database for admin panel
func GetAllPages() []Page {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.PageCollection)

	result := []Page{}

	err := col.Find(nil).Select(bson.M{"content": 0}).Sort("title").All(&result)
	if err != nil {
		log.Panic(err)
	}

	return result
}

// PageCount return the total number of pages
func PageCount() int {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.PageCollection)

	count, err := col.Find(nil).Count()
	if err != nil {
		log.Panic(err)
	}

	return count

}

// DeletePage deletes the given user from the database
func DeletePage(id bson.ObjectId) {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.PageCollection)

	err := col.Remove(bson.M{"_id": id})
	if err != nil {
		log.Panic(err)
	}
}
