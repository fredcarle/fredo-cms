package model

import (
	"log"

	"gitlab.com/fredcarle/fredo-cms/utils/config"
	"gitlab.com/fredcarle/fredo-cms/utils/database"
	"gopkg.in/mgo.v2/bson"
)

const (
	// UserRole is used for queries on the main site
	UserRole = "user"
	// AdminRole is used for queries in the admin panel
	AdminRole = "admin"
)

// User contains the information relevant to a specific user
type User struct {
	ID        bson.ObjectId `bson:"_id"`
	Firstname string
	Lastname  string
	Username  string
	Password  []byte
	Email     string
	Role      string
	Posts     AuthorsPosts
}

// AuthorsPosts is a struct containing an others posts id and the total number
// of posts
type AuthorsPosts struct {
	Total int
	IDs   []bson.ObjectId `bson:"posts_id"`
}

// PostAuthor is the struct giving the relevant author details for a given post
type PostAuthor struct {
	Firstname string
	Lastname  string
	Username  string
}

// CommentAuthor is the struct giving the relevant comment author details
// for a given comment
type CommentAuthor struct {
	Firstname string
	Lastname  string
	Username  string
	Email     string
}

// UserEdit is the struct containing the relevant details for user editing
type UserEdit struct {
	SiteURL string
	User    User
	New     bool
}

// UsersEdit is the struct containing all the editable users
type UsersEdit struct {
	SiteURL string
	Users   []User
}

// GetUser extracts the user details from the database
func (u *User) GetUser() {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.UserCollection)

	err := col.Find(bson.M{"_id": u.ID}).Select(bson.M{"password": 0}).One(u)
	if err != nil {
		log.Panic(err)
	}
}

// GetAllUsers extracts the users details from the database
func GetAllUsers() []User {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.UserCollection)

	result := []User{}
	err := col.Find(nil).Select(bson.M{"password": 0, "posts.ids": 0}).Sort("firstname").All(&result)
	if err != nil {
		log.Panic(err)
	}
	return result
}

// GetAuthor extracts the author's details from the database
func GetAuthor(a interface{}) {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.UserCollection)

	postAuthor, ok := a.(*PostAuthor)
	if ok {
		err := col.Find(bson.M{"username": postAuthor.Username}).One(postAuthor)
		if err != nil {
			log.Panic(err)
		}
	}

	commentAuthor, ok := a.(*CommentAuthor)
	if ok {
		err := col.Find(bson.M{"username": commentAuthor.Username}).One(commentAuthor)
		if err != nil {
			log.Panic(err)
		}
	}

}

// GetUserPassword extracts the user password from the database
func (u *User) GetUserPassword() error {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.UserCollection)

	err := col.Find(bson.M{"username": u.Username}).Select(bson.M{"password": 1, "firstname": 1, "username": 1}).One(u)
	if err != nil {
		log.Printf("[LOGIN] Error while trying to find password: %v", error.Error(err))
		return err
	}

	return nil
}

// GetUserRole extracts the user role from the database
func (u *User) GetUserRole() {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.UserCollection)

	err := col.Find(bson.M{"username": u.Username}).Select(bson.M{"role": 1, "username": 1}).One(u)
	if err != nil {
		log.Panic(err)
	}
}

// SaveUser saves the user details in the database
func (u *User) SaveUser() {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.UserCollection)

	if u.Password == nil {
		var result struct{ Password []byte }
		err := col.Find(bson.M{"_id": u.ID}).Select(bson.M{"password": 1, "_id": 0}).One(&result)
		if err != nil {
			log.Panic(err)
		}
		u.Password = result.Password
	}
	_, err := col.Upsert(bson.M{"_id": u.ID}, bson.M{"$set": bson.M{"email": u.Email,
		"firstname": u.Firstname, "lastname": u.Lastname, "password": u.Password,
		"role": u.Role, "username": u.Username}})
	if err != nil {
		log.Panic(err)
	}
}

// UpdateUserPostList add the number of post written by an author to that authors
// document
func UpdateUserPostList(username string, id bson.ObjectId) {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.UserCollection)

	_, err := col.Upsert(bson.M{"username": username}, bson.M{"$addToSet": bson.M{"posts.posts_id": id}})
	if err != nil {
		log.Panic(err)
	}

	var result struct {
		Posts struct {
			IDs []bson.ObjectId `bson:"posts_id"`
		}
	}
	err = col.Find(bson.M{"username": username}).Select(bson.M{"posts.posts_id": 1, "_id": 0}).One(&result)
	if err != nil {
		log.Panic(err)
	}
	_, err = col.Upsert(bson.M{"username": username}, bson.M{"$set": bson.M{"posts.total": len(result.Posts.IDs)}})
	if err != nil {
		log.Panic(err)
	}

}

// UserCount return the total number of users
func UserCount() int {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.UserCollection)

	count, err := col.Find(nil).Count()
	if err != nil {
		log.Panic(err)
	}

	return count

}

// DeleteUser deletes the given user from the database
func DeleteUser(username string) {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.UserCollection)

	err := col.Remove(bson.M{"username": username})
	if err != nil {
		log.Panic(err)
	}
}

// IsFirstUser return true if the user being registered is the first one created
func IsFirstUser() bool {
	count := UserCount()
	if count > 0 {
		return false
	}

	return true
}
