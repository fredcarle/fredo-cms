package model

import (
	"log"
	"time"

	"gitlab.com/fredcarle/fredo-cms/utils/config"
	"gitlab.com/fredcarle/fredo-cms/utils/database"
	"gopkg.in/mgo.v2/bson"
)

// PasswordReset is the struct containing the relevant details for
// resetting the user's password
type PasswordReset struct {
	ID         bson.ObjectId `bson:"_id"`
	Email      string
	Username   string
	ExpiryDate time.Time
}

// EmailExist checks if a given email address bellongs to an existing user
func EmailExist(email string) (bool, string) {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.UserCollection)

	var result struct {
		Username string
	}

	err := col.Find(bson.M{"email": email}).Select(bson.M{"username": 1, "_id": 0}).One(&result)
	if err != nil {
		log.Printf("[EMAIL] Error trying to find email: %v", err)
		return false, ""
	}

	return true, result.Username
}

// CreatePasswordResetLink adds an active password reset link for a given user
func CreatePasswordResetLink(pr *PasswordReset) bool {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.PasswordReset)

	_, err := col.Upsert(bson.M{"_id": pr.ID}, pr)

	if err != nil {
		log.Printf("[PASSWORD] Error creating password reset link: %v", err)
		return false
	}

	return true

}

// GetPasswordReset gets the password reset info associated with
// the given password reset ID
func GetPasswordReset(id string) PasswordReset {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.PasswordReset)

	pr := new(PasswordReset)

	err := col.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(&pr)

	if err != nil {
		log.Printf("[PASSWORD] Error getting password reset info: %v", err)
	}

	return *pr
}

// SaveNewPassword saves the new user password for the given username
func SaveNewPassword(username string, password []byte) {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.UserCollection)

	err := col.Update(bson.M{"username": username}, bson.M{"$set": bson.M{"password": password}})
	if err != nil {
		log.Panic(err)
	}
}

// RemovePasswordReset removes all the password reset links for a given username
func RemovePasswordReset(username string) {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.PasswordReset)

	_, err := col.RemoveAll(bson.M{"username": username})

	if err != nil {
		log.Panic(err)
	}
}
