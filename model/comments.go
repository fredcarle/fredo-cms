package model

import (
	"log"
	"sort"
	"time"

	"gitlab.com/fredcarle/fredo-cms/utils/config"
	"gitlab.com/fredcarle/fredo-cms/utils/database"

	"gopkg.in/mgo.v2/bson"
)

// Comment is the struct containing the comment information
type Comment struct {
	ID       bson.ObjectId `bson:"_id"`
	Author   CommentAuthor
	AuthorIP string `bson:"author_ip"`
	Date     time.Time
	Status   string
	Content  string
	On       string `bson:"post_slug"`
	IsReply  bool
	ReplyTo  bson.ObjectId   `bson:"replyto_id,omitempty"`
	Replies  []bson.ObjectId `bson:"reply_ids,omitempty"`
}

// ByDate implements sort.Interface for []Comment based on
// the Date field.
type ByDate []Comment

func (a ByDate) Len() int           { return len(a) }
func (a ByDate) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByDate) Less(i, j int) bool { return a[i].Date.Unix() > a[j].Date.Unix() }

// SaveComment save the comment struct in the database
func (comment *Comment) SaveComment() {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	// Save the comment to the comment collection
	col := sessionCopy.DB(config.DB.Name).C(database.CommentCollection)

	_, err := col.Upsert(bson.M{"_id": comment.ID}, comment)
	if err != nil {
		log.Panic(err)
	}

	// Update the replies list of the comment this comment was a reply to
	if comment.IsReply && comment.ReplyTo != "" {
		_, err = col.Upsert(bson.M{"_id": comment.ReplyTo}, bson.M{"$addToSet": bson.M{"reply_ids": comment.ID}})
		if err != nil {
			log.Panic(err)
		}
	}

	// increment the total number of comments for the corresponding post in
	// the post collection
	col = sessionCopy.DB(config.DB.Name).C(database.PostCollection)

	_, err = col.Upsert(bson.M{"slug": comment.On}, bson.M{"$inc": bson.M{"totalcomments": 1}})
	if err != nil {
		log.Panic(err)
	}

}

// UpdateComment updates the comment content and status
func (comment *Comment) UpdateComment() {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	// Save the comment to the comment collection
	col := sessionCopy.DB(config.DB.Name).C(database.CommentCollection)

	_, err := col.Upsert(bson.M{"_id": comment.ID}, bson.M{"$set": bson.M{"content": comment.Content, "status": comment.Status}})
	if err != nil {
		log.Panic(err)
	}

}

// GetOneComment gets the comment from the database with the corresponding ID
func GetOneComment(id bson.ObjectId) Comment {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.CommentCollection)

	result := new(Comment)

	err := col.Find(bson.M{"_id": id}).Select(bson.M{"author_ip": 0}).One(&result)
	if err != nil {
		log.Panic(err)
	}

	return *result
}

// GetComments gets the relevent comments using the post slug
func GetComments(slug string) []Comment {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.CommentCollection)

	result := []Comment{}

	if slug != "" {
		err := col.Find(bson.M{"post_slug": slug, "isreply": false, "status": "approved"}).Sort("-date").All(&result)
		if err != nil {
			log.Panic(err)
		}
	} else {
		err := col.Find(nil).Sort("-date").All(&result)
		if err != nil {
			log.Panic(err)
		}
	}

	return result

}

// GetReplies gets the relevent replies using the comment ids
func GetReplies(replies []bson.ObjectId) []Comment {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.CommentCollection)

	result := []Comment{}

	for _, reply := range replies {
		comment := new(Comment)
		err := col.Find(bson.M{"_id": reply, "isreply": true, "status": "approved"}).One(&comment)
		if err != nil {
			log.Panic(err)
		}
		result = append(result, *comment)
	}

	sort.Sort(ByDate(result))

	return result

}

// DeleteComment removes the comment from the database and updates the total
// comment of the related post
func (comment *Comment) DeleteComment() {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	// Remove the comment from the comment collection
	col := sessionCopy.DB(config.DB.Name).C(database.CommentCollection)

	err := col.Remove(bson.M{"_id": comment.ID})
	if err != nil {
		log.Panic(err)
	}

	// Update the replies list of the comment this comment was a reply to
	if comment.IsReply && comment.ReplyTo != "" {
		_, err = col.Upsert(bson.M{"_id": comment.ReplyTo}, bson.M{"$pull": bson.M{"reply_ids": comment.ID}})
		if err != nil {
			log.Panic(err)
		}
	}

	// decrement the total number of comments for the corresponding post in
	// the post collection
	col = sessionCopy.DB(config.DB.Name).C(database.PostCollection)

	_, err = col.Upsert(bson.M{"slug": comment.On}, bson.M{"$inc": bson.M{"totalcomments": -1}})
	if err != nil {
		log.Panic(err)
	}
}

// CommentCount return the total number of users
func CommentCount() int {
	sessionCopy := database.Mongo.Copy()
	defer sessionCopy.Close()

	col := sessionCopy.DB(config.DB.Name).C(database.CommentCollection)

	count, err := col.Find(nil).Count()
	if err != nil {
		log.Panic(err)
	}

	return count

}

// // CommentCount returns the number of comment for a given article
// func CommentCount(col *mgo.Collection, slug string) int {
//
// }
